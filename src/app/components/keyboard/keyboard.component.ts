import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent implements OnInit {

  public azertyKeyboard: string[][] = [
    ['A','Z','E','R','T','Y','U','I','O','P'],
    ['Q','S','D','F','G','H','J','K','L','M'],
    ['W','X','C','V','B','N']
  ]

  public letter?: any;

  @Output()
  public letterEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  @HostListener('click', ['$event.target'])
  public clickOnLetter(letter:any){
    this.letter = (letter as HTMLElement).textContent;
    this.letterEvent.emit(this.letter);
    console.log(letter, this.letter);

  }

}
