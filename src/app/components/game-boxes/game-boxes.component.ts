import { Component, Input, IterableChanges, IterableDiffer, IterableDiffers, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-game-boxes',
  templateUrl: './game-boxes.component.html',
  styleUrls: ['./game-boxes.component.scss']
})
export class GameBoxesComponent implements OnInit {

  public nbLetters: Number[] = [];
  public maxAttempts: Number[] = [];

  @Input()
  public lettersClick: string[] = [];

  constructor(private formBuilder: FormBuilder) {
    // Creation line & letters boxes in static
    // TODO dynamize number letters & maxAttempts
    this.nbLetters = [...Array(6).keys()];
    this.maxAttempts = [...Array(5).keys()];

  }

  public wordForm = this.formBuilder.group({
    letter0: ['', Validators.required],
    letter1: ['', Validators.required],
    letter2: ['', Validators.required],
    letter3: ['', Validators.required],
    letter4: ['', Validators.required],
    letter5: ['', Validators.required],
  })

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.wordForm.value);
    console.log(this.lettersClick);
  }

  ngOnInit(): void {
  }

  // ngOnChanges(changes: SimpleChanges): void {
  //   let lettersClick = this.lettersClick.slice();
  //   for(let letter of this.lettersClick){
  //     console.log(letter);
  //   }
  // }

  ngDoCheck(){

      for(let [index, letter] of this.lettersClick.entries()){
        this.wordForm.controls['letter'+ index].setValue(letter);
        console.log(this.wordForm.controls['letter']);
      }

  }
}
