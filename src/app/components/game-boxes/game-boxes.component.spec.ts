import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoxesComponent } from './game-boxes.component';

describe('GameBoxesComponent', () => {
  let component: GameBoxesComponent;
  let fixture: ComponentFixture<GameBoxesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoxesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
