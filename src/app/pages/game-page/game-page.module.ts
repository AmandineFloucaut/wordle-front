import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeyboardComponent } from '@/app/components/keyboard/keyboard.component';
import { GamePageComponent } from './game-page.component';
import { FormGameComponent } from '@/app/components/form-game/form-game.component';
import { GameBoxesComponent } from '@/app/components/game-boxes/game-boxes.component';
import { LucideAngularModule, LogIn, SkipBack } from 'lucide-angular';
import { ReactiveFormsModule } from '@angular/forms';
import { GamePageRoutingModule } from './game-page-routing.module';


@NgModule({
  declarations: [GamePageComponent, KeyboardComponent, FormGameComponent, GameBoxesComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LucideAngularModule.pick({ LogIn, SkipBack }),
    GamePageRoutingModule
  ]
})
export class GamePageModule { }
