import { Component, Input, OnInit } from '@angular/core';
import { KeyboardComponent } from '@/app/components/keyboard/keyboard.component'

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.scss']
})
export class GamePageComponent implements OnInit {

  //public azertyKeyboard: any;
  public lettersClick: string[] = [];

  constructor() { }

  ngOnInit(): void {
    //console.log(azertyKeyboard);
  }

  public constructWordGuesses(letter: string): string[] {
    this.lettersClick.push(letter);
    console.log(this.lettersClick);
    
    return this.lettersClick;
  }

}
