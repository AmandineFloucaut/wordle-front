import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamePageComponent } from './pages/game-page/game-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'game', pathMatch: 'full'},
  { path: 'game', loadChildren: () => import('@/pages/game-page/game-page.module').then((module) => module.GamePageModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
