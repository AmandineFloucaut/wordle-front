import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
//import { GameBoxesComponent } from './components/game-boxes/game-boxes.component';
//import { KeyboardComponent } from './components/keyboard/keyboard.component';
//import { GamePageComponent } from './pages/game-page/game-page.component';

import { LucideAngularModule, LogIn, SkipBack } from 'lucide-angular';
//import { FormGameComponent } from './components/form-game/form-game.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
